# Deploy Microservices Application with best practices 2024

### Pre-Requisites
minikube/kubectl installed

Linode account

#### Project Outline
In this project we will deploy the application using best practices for the previously deployed microservice shop in the below

https://gitlab.com/FM1995/deploy-microservices-application-2024

#### Getting Started

In the previous project we created K8 config files with bad practice despite the microservice running, one of the best practices is using tagging, liveness probe on each container, we can also perform health checks with liveness probe. Since it is a container attribute we can configure it on the container level

Lets begin by adding it as seen below

![Image1](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image1.png?ref_type=heads)

Another best practice is implementing Readiness Probe for each container which lets us know if the container is ready to recieve traffic

Without readiness probe, k8s assumes the app is ready to receive traffic as soon as the container starts

Below is how we can add

![Image2](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image2.png?ref_type=heads)

For redis can configure the below

![Image3](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image3.png?ref_type=heads)

Can also incorporate the below

initialDelaySeconds: 5: This setting tells Kubernetes to wait for 5 seconds after the container has started before performing the first liveness probe. This delay allows your application to start up before Kubernetes begins checking its health.

initialDelaySeconds: 5: Similar to the liveness probe, this setting gives the container 5 seconds after starting before the readiness checks begin.

![Image4](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image4.png?ref_type=heads)

We can also incorporate HTTP probes
GET request.


path: /health: This specifies the path that the HTTP GET request will target. The probe will make a request to the /health endpoint of the application running inside the container.

![Image5](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image5.png?ref_type=heads)

Next we need to configure Resource request for each container, where it has two types of resources, CPU and memory, requests is what the container is guaranteed to get

Here we have configured the below

cpu: 100m: The container requests 100 millicores of CPU, equating to 10% of a single CPU core. This request aids Kubernetes in pod placement but does not limit the CPU usage; the container may use more CPU if available

memory: 64Mi: The container requests 64 Mebibytes of memory. This is used by Kubernetes for scheduling the pod on a node with sufficient memory and, similar to CPU requests, is not a strict limit; the container can use more memory if it's available on the node

![Image6](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image6.png?ref_type=heads)

What is the container will consume more that the requests resources, if not limited could consume all the Node’s resources

So we have to ensure each container does not over consume and therefore provide resource limits

And can define it in the below

![Image7](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image7.png?ref_type=heads)

Let’s say some microservices need more resources than others, like adservice, we can definitely increase that

![Image8](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image8.png?ref_type=heads)

And for redis, it will need more memory and less cpu

Can do the below for redis

![Image9](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image9.png?ref_type=heads)

Another best practice is not to expose NodePort
As it exposes the application to a security risk, it exposes a node port on the cluster and increases the attack surface. So it is best to have one entry point which can divert traffic. So we can use a LoadBalancer which can provide a single point of entry to distribute traffic into the internal service.

Just like the below

 ![Image10](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image10.png?ref_type=heads)

Another best practice is to deploy with multiple replicas, by increasing the replicas count we  ensure no downtime.

![Image11](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image11.png?ref_type=heads)

Let’s test the deployment

```
kubectl delete deployments --all -n microservices
```

And then re-apply the best-config.yaml

```
kubectl apply -f best-practice-config.yaml -n microservices
```

![Image12](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image12.png?ref_type=heads)

```
kubectl get pod -n microservices
```

![Image13](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image13.png?ref_type=heads)

Can now access using the below

```
kubectl get svc -n microservices
```

![Image14](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image14.png?ref_type=heads)

Can now access on the External IP

![Image15](https://gitlab.com/FM1995/deploy-microservices-application-with-best-practices-2024/-/raw/main/Images/Image15.png?ref_type=heads)










